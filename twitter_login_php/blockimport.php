<?php 

    class blockimport 
    {
        public function __construct()
        {
            
        }

        public function import_block($file, $connection)
        {
            $blocked_amount = 0;
            if($file != null)
            {
                $blocked_ids = array_map('str_getcsv', file($file));
                $blocked_ids = explode(";", $blocked_ids[0][0]);
    
                foreach($blocked_ids as $blocked_id){
                    //echo $blocked_id .'<br />';
                    $block = $connection->post("blocks/create", ['user_id' => $blocked_id]);
                    $blocked_amount++;
                }
            }
            return $blocked_amount;
        }

        public function import_unblock($file, $connection)
        {
            $unblocked_amount = 0;
            if($file != null)
            {
                $unblocked_ids = array_map('str_getcsv', file($file));
                $unblocked_ids = explode(";", $unblocked_ids[0][0]);
    
                foreach($unblocked_ids as $unblocked_id){
                    //echo $blocked_id .'<br />';
                    $unblock = $connection->post("blocks/destroy", ['user_id' => $unblocked_id]);
                    $unblocked_amount++;
                }
            }
            return $unblocked_amount;
        }
    }
?>