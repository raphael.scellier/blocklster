<!DOCTYPE html>
<html>
	<head>
		<!-- title of our page -->
		<title>BlockLSTER</title>

		<link rel="icon" type="image/png" href="assets/minilster.png" />
		<meta name="description" content="BlockLSTER vous permet de gérer facilement les imports et exports de listes de blocages sur Twitter." />

		<!-- include fonts -->
		<link href="https://fonts.googleapis.com/css?family=Coda" rel="stylesheet">

		<!-- need this so everything looks good on mobile devices -->
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

		<!-- css styles for our login page-->
		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/login.css" rel="stylesheet" type="text/css">
		<link href="css/footer.css" rel="stylesheet" type="text/css">
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

		<!-- jquery -->
		<script type="text/javascript" src="js/jquery.js"></script>

		<!-- include our loader overlay script -->
		<script type="text/javascript" src="js/loader.js"></script>

		
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

		<script>
			$( function() { // do things when the document is ready
				// initialize our loader overlay
				//loader.initialize();

				$( '#logout_link' ).on( 'click', function() { // on click for our logout link
					// show our loading overlay
					//loader.showLoader();

					// server side logout
					$.ajax( {
						url: 'logout.php',
						type: 'post',
						dataType: 'json',
						success: function( data ) {
							//loader.hideLoader();
							window.location.href = "index.php";
						}
					} );
				} );
			} );
		</script>
	</head>
<body>
	<!--<div id="page-container">
	<div id="content-wrap">-->
		
<?php
	// using sessions to store token info
	session_start();

	// require config and twitter helper
	require 'config.php';
	require 'twitter-login-php/autoload.php';
	include("blockimport.php");

	

	// use our twitter helper
	use Abraham\TwitterOAuth\TwitterOAuth;

	if ( isset( $_SESSION['twitter_access_token'] ) && $_SESSION['twitter_access_token'] ) { // we have an access token
		$isLoggedIn = true;	
	} elseif ( isset( $_GET['oauth_verifier'] ) && isset( $_GET['oauth_token'] ) && isset( $_SESSION['oauth_token'] ) && $_GET['oauth_token'] == $_SESSION['oauth_token'] ) { // coming from twitter callback url
		// setup connection to twitter with request token
		$connection = new TwitterOAuth( CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret'] );
		
		// get an access token
		$access_token = $connection->oauth( "oauth/access_token", array( "oauth_verifier" => $_GET['oauth_verifier'] ) );

		// save access token to the session
		$_SESSION['twitter_access_token'] = $access_token;

		// user is logged in
		$isLoggedIn = true;
	} else { // not authorized with our app, show login button
		// connect to twitter with our app creds
		$connection = new TwitterOAuth( CONSUMER_KEY, CONSUMER_SECRET );

		// get a request token from twitter
		$request_token = $connection->oauth( 'oauth/request_token', array( 'oauth_callback' => OAUTH_CALLBACK ) );

		// save twitter token info to the session
		$_SESSION['oauth_token'] = $request_token['oauth_token'];
		$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

		// user is logged in
		$isLoggedIn = false;
	}

	if ( $isLoggedIn ) { // logged in
		// get token info from session
		$oauthToken = $_SESSION['twitter_access_token']['oauth_token'];
		$oauthTokenSecret = $_SESSION['twitter_access_token']['oauth_token_secret'];

		// setup connection
		$connection = new TwitterOAuth( CONSUMER_KEY, CONSUMER_SECRET, $oauthToken, $oauthTokenSecret );

		// user twitter connection to get user info
		$user = $connection->get( "account/verify_credentials", ['include_email' => 'true'] );

		if ( property_exists( $user, 'errors' ) ) { // errors, clear session so user has to re-authorize with our app
	    	$_SESSION = array();
	    	header( 'Refresh:0' );
		} else { // display user info in browser
			
			// get blocked accounts ids
			$blocked_accounts = $connection->get( "blocks/ids", ['include_email' => 'true', 'stringify_ids' => 'true'] );
			$array_blocks = (array) $blocked_accounts->ids;
			if (!is_dir('accounts/exports/' . $user->screen_name))
			{
				mkdir('accounts/exports/' . $user->screen_name);
			}
			$fp = fopen('accounts/exports/' . $user->screen_name . '/file.csv', 'w');


			fputcsv($fp, $array_blocks,";");

			fclose($fp);

			?>
			<div class="profile-card">
				<div class="profile-picture">
					<img src="<?php echo str_replace("_normal", "", $user->profile_image_url); ?>" class="profile-picture-picture" />
				</div>
				<div class="profile-info">
					<div class="profile-username">
						<b>Nom :</b> <?php echo $user->name; ?>
					</div>
					<br/>
					<div class="profile-username">
						<b>Nom d'utilisateur :</b> <?php echo $user->screen_name; ?>
					</div>
				</div>
				<div class="logout">
					<div id="logout_link" class="a-default">Déconnexion</div>
				</div>
			</div>
			<div class="import-content-container">
				<div class="site-content-centered">
				<div class="site-content-section">
						<div class="site-content-section-inner">
							<b>Import de blocklst :</b> 
							<form method="post" enctype="multipart/form-data" class="import-blocklist">
								<input type="file" name="file">	
								<input type="submit" name="sub" value="Import">	
							</form>
						</div>
					</div>
					<div class="site-content-section">
						<div class="site-content-section-inner">
							<b>Import de déblocklst :</b> 
							<form method="post" enctype="multipart/form-data" class="import-blocklist">
								<input type="file" name="file">	
								<input type="submit" name="subdeblok" value="Import">	
							</form>
						</div>
					</div>
					<div class="site-content-section">
						<div class="site-content-section-inner">
							<b>Export de blocklst :</b> <a href="<?php echo 'http://rafficelle.fr/site_avec_loca/twitter_login_php/accounts/exports/' . $user->screen_name . '/file.csv'; ?>">Cliquez ici</a>
						</div>
					</div>
					<div class="site-content-section">
						<div class="site-content-section-inner">
							<b>Le truc pour le consultant (trouver un nom plus tard)</b> <a href="siteLC/Liste.php">Cliquez ici</a>
						</div>
					</div>
					<?php
					$blockimport =  new blockimport();

					if (isset($_POST['sub'])){
						$blocked_amount = $blockimport->import_block($_FILES['file']['tmp_name'], $connection);?>
						<div class="site-content-section-inner">
							<b><?php echo 'Vous avez bloqué ' . $blocked_amount . ' compte(s) avec succès.';?></b>
						</div><?php
					}

					if (isset($_POST['subdeblok'])){
						$blocked_amount = $blockimport->import_unblock($_FILES['file']['tmp_name'], $connection);?>
						<div class="site-content-section-inner">
							<b><?php echo 'Vous avez débloqué ' . $blocked_amount . ' compte(s) avec succès.';?></b>
						</div><?php
					}?>
				</div>
			</div>
			
	    	<?php
	    }
	} else {  // not logged in, get and display the login with twitter link
		$url = $connection->url( 'oauth/authorize', array( 'oauth_token' => $request_token['oauth_token'] ) );
		?>
		<div class="site-content-container">
			<div class="site-content-centered">
				<div class="site-content-section">
					<div class="site-content-section-inner">
						<div class="section-heading">Connexion</div>
						<div class="section-action-container">
							<div id="error_message_twitter_php" class="error-message">
								<?php if ( 'fail' == $twitterPreLoginData['status'] ) : // twitter fail ?>
									<div>
										<?php echo $twitterPreLoginData['message']; ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="section-action-container">
							<a href="<?php echo $url ;?>" class="a-tw">
								<div class="tw-button-container">
									<div class="tw-button-logo">
										<img src="assets/twitter.png" width="32" height="27"></img>
									</div>
									<div class="tw-button-text">
										Se connecter avec Twitter
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!--</div>
	</div>-->
		<?php
	}include('footer.php');?>
</body>
</html>