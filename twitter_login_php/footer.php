

<footer id="footer">
		<div class="container">
			<div class="row text-center text-xs-center text-sm-left text-md-left">
				<div class="col-xs-12 col-sm-4 col-md-4">
					<h5>Liens utiles</h5>
					<ul class="list-unstyled quick-links">
						<li><a href="FAQ.php"><i class="fa fa-angle-double-right"></i>FAQ</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
                    <h5>Liens utiles</h5>
					<ul class="list-unstyled quick-links">
						<li><a href="FAQ.php"><i class="fa fa-angle-double-right"></i>FAQ</a></li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
                    <h5>Liens utiles</h5>
					<ul class="list-unstyled quick-links">
						<li><a href="FAQ.php"><i class="fa fa-angle-double-right"></i>FAQ</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
					<ul class="list-unstyled list-inline social text-center">
						<li class="list-inline-item"><a href="https://twitter.com/raficelle"><i class="fa fa-twitter"></i></a></li>
						<li class="list-inline-item"><a href="https://twitter.com/locxro"><i class="fa fa-twitter"></i></a></li>
					</ul>
				</div>
				<hr>
			</div>	
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
					<p><u><a href="https://www.blockLSTER.fr/">BlockLSTER</a></u> est une marque très déposée.</p>
					<p class="h6">© Tous droits réservés.<a class="text-green ml-2" href="https://www.blockLSTER.fr/" target="_blank">BlockLSTER</a></p>
				</div>
				<hr>
			</div>	
		</div>
</footer>