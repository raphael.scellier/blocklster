<!DOCTYPE html>
<html>
	<head>
		<!-- title of our page -->
		<title>BlockLSTER</title>

		<link rel="icon" type="image/png" href="assets/minilster.png" />
		<meta name="description" content="Réponses a vos questions les plus fréquemment posées." />

		<!-- include fonts -->
		<link href="https://fonts.googleapis.com/css?family=Coda" rel="stylesheet">

		<!-- need this so everything looks good on mobile devices -->
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

		<!-- css styles for our login page-->
		<link href="css/global.css" rel="stylesheet" type="text/css">
		<link href="css/login.css" rel="stylesheet" type="text/css">
		<link href="css/footer.css" rel="stylesheet" type="text/css">
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

		<!-- jquery -->
		<script type="text/javascript" src="js/jquery.js"></script>

		<!-- include our loader overlay script -->
		<script type="text/javascript" src="js/loader.js"></script>

		
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </head>
    <body>
        <h2>A quoi ça sert ?</h2>
        <p>BlockLSTER est un site vous permettant d'importer et exporter des listes de blocage, afin de les partager entre plusieurs comptes.</p>
        <h2>Pourquoi ? Twitter le fait déjà, non ?</h2>
        <p>Non. Twitter a suspendu cette superbe fonctionnalité il y a peu.</p>
        <h2>Comment ça fonctionne ?</h2>
        <h3>Export</h3>
        <p>Une fois connecté.e.s sur la page dédiée, il suffit de cliquer sur le bouton "Export", et un fichier au format CSV est automatiquement téléchargé sur votre appareil. Celui-ci contient les identifiants uniques de chaque personne que vous avez bloqué.e.s</p>
        <h3>Import</h3>
        <p>Pour importer, il faut dans un premier temps sélectionner un fichier CSV exporté au préalable par notre outil. Une fois chose faite, cliquez sur "Import". On se charge du reste ! Vous pouvez observer votre liste de comptes bloqués s'agrandir.</p>
        <?php include('footer.php'); ?>
    </body> 
</html>
