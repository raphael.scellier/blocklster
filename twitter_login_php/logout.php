<?php
	// load up global things
	
    session_start();
    
    

	// destroy the session (clears all session data)
    session_destroy();
    
    echo json_encode( 
		array(
			'status' => 'ok',
		)
	);